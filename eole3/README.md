# Outil d'aide au déploiement d'Eole³

## Présentation de l'outil

L'outil [tools](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/) sert à générer les différents fichiers nécessaires au déploiement des charts pour les services **Eole³**. 
La procédure d'utilisation est la suivante : 

- Adapter les valeurs par défaut à votre configuration
- Générer les fichiers indispensables
- Déployer la configuration de base

## Prérequis

Il vous faut disposer : 

- d'un cluster Kubernetes
- du fichier d'accès à l'API Kubernetes :`export KUBECONFIG=kubeconfig.yaml`
- de la commande [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
- de la commande [helm](https://helm.sh/docs/intro/install/)
- de la commande [jq](https://stedolan.github.io/jq/)
- des dépendances python3 `click` et `jinja2`
- d'un nom de domaine
- d'un serveur DNS qui résoud l'enregistrement wilcard pour le domaine
- d'un certificat wildcard reconnu pour ce domaine. Il est impossible d'utiliser un certificat auto-signé

## Description du socle de base

Les différents services applicatifs inclus dans la base sont listés dans le tableau ci-dessous. La boîte est le service principal, toujours déployé, les autres services sont optionnels et activables au moment de la configuration.


| Applications  | Description                                |
| ------------- | ------------------------------------------ |
| Laboite       | Portail d'accès aux services Eole³         |
| Agenda        | Agenda partagé                             |
| Blog          | Affichage des articles de blog             |
| Blogapi       | API REST d'accès aux articles de blog      |
| Frontnxt      | Aiguilleur d'accès aux nuages (Nextcloud©) |
| Lookup-server | Moteur de recherche des utilisateurs       |
| Mezig         | Publication des compétences                |
| Radicale      | Serveur de calendrier (caldav)             |
| Sondage       | Création et gestion de sondages            |

Les entrées du tableau ci-dessous correspondent aux services d'infrastructure nécessaires à l'exploitations des applications précédentes (ils sont toujours déployés).

| Nom            | Description                                                       |
| -------------- | ----------------------------------------------------------------- |
| Keycloak       | Serveur d'authentification unique                                 |
| Mongodb        | Base de données en mode replicaset                                |
| Minio          | Serveur de stockage S3                                            |
| Nginx          | Contrôleur ingress par défaut                                     |

## Les services additionnels

Grâce à cet outil, certaines applications peuvent être intégrées au socle de base. Cette liste est destinée à s'étoffer. 

| Nom            | Description                          | SSO                   | Statut | 
| -------------- | ------------------------------------ | --------------------- | -------|
| CodiMD         | Serveur d'édition collaborative      | configuré             | stable |
| Latelier       | Serveur de gestion de projets        | configuré             | beta   |
| Nextcloud      | Partage de documents dans les nuages | configuré             | stable |
| RocketChat     | Serveur de discussion                | manuel                | stable |
| Gitea 	     | Forge logicielle                     | configuré             | stable |
| Drawio         | Outil de diagramme en ligne          | sans authentification | stable |
| Excalidraw     | Outil de dessin collaboratif         | sans authentification | beta   |
| Wikijs         | Plateforme de Wiki                   | manuel                | stable |
| Filepizza      | Transfert de fichiers                | sans authentification | beta   |
| Screego        | Partage d'écran                      | sans authentification | beta   |
| Discourse      | Serveur de forums et discussion      | manuel                | beta   |

## Les outils d'administration

Cet outil permet également d'intégrer des outils d'administration. Cette liste est destinée à s'étoffer. 

| Nom              | Description                              | Status |
| ---------------- | ---------------------------------------- | ------ |
| SuperCRUD        | UI d'administration de la base mongodb laboite  | beta |
| Promotheus       | Collecteur de métriques                  | deprecated (use prometheus-stack)|
| Grafana          | Visualiseur de métriques                 | deprecated (use prometheus-stack)|
| Promotheus-stack | Collecteur de métriques (prometheus-operator + grafana)      | stable |
| Loki-stack       | Visualiseur de logs et exporteur de logs | stable |
| Promtail         | Collecteur de logs                       | stable |

## Démarrage rapide 

### Installation du socle de base

1. Cloner le dépôt : `git clone https://gitlab.mim-libre.fr/EOLE/eole-3/tools.git && cd tools`
2. Éditer le fichier `vars.ini` et l'adapter à votre environnement. Il faut au minimum adapter la valeur de la variable `domain` à votre nom de domaine 
3. Lancer la génération des fichiers avec la commande `./build gen-socle`
4. Copier la clef privée (`tls.key`) et le certificat (`tls.crt`) wildcard de votre nom de domaine dans le répertoire `./install`
5. Déployer la configuration avec les commandes suivantes  `cd ./install && bash deploy`

### Mise à jour du socle de base

Pour mettre à jour le socle, il vous faut : 
1. Éditer le fichier `vars.ini` et mettre à jour les options
2. Lancer la génération des fichiers avec la commande `./build gen-socle`
3. Mettre à jour la configuration avec les commandes suivantes  `cd ./install && bash update`

### Installation d'un service additionnel (exemple pour CodiMD)

1. Éditer éventuellement le fichier `addons/codimd/codimd-vars.ini`
3. Lancer la génération des fichiers avec la commande `./build gen-addon -n codimd`
4. Déployer la configuration avec les commandes suivantes  `cd ./install/addons/codimd && bash deploy`

### Installation d'un outil d'administration (exemple pour SuperCRUD)

1. Éditer éventuellement le fichier `admin-tools/supercrud/supercrud-vars.ini`
3. Lancer la génération des fichiers avec la commande `./build gen-admin-tool -n supercrud`
4. Déployer la configuration avec les commandes suivantes  `cd ./install/admin-tool/supercrud && bash deploy`

## Configuration avancée

Vous pouvez spécifier les fichiers de configurations utilisés pour la génération grâce aux option de la commande `build`

### Le socle de base

```
# ./build --help gen-socle 
Usage: build [OPTIONS] COMMAND [ARGS]...

Options:
  -c, --configfile PATH          Configuration values (default: vars.ini)
  -cc, --clusterconfigfile PATH  Cluster configuration values (default:
                                 cluster-vars.ini)
  -o, --outputdir TEXT           Output Directory (default: ./install)
  --help                         Show this message and exit.
```
Exemple : 
`./build -c path/to/my-vars.ini -cc /path/to/my-cluster-vars.ini -o /path/to/my-install gen-socle`

### Les service additionnels

```
# ./build gen-addon --help
Usage: build gen-addon [OPTIONS]

  Generate addon

Options:
  -n, --name TEXT           Addon name
  -ac, --addon-config PATH  Addon configuration values
  --help                    Show this message and exit.
```
Exemple pour CodiMD: 
`./build -c path/to/my-vars.ini -cc path/to/my-cluster-vars.ini gen-addon -n codimd -ac path/to/my-codimd-vars.ini`

### Les outils d'administration

```
# ./build gen-admin-tool --help
Usage: build gen-admin-tool [OPTIONS]

  Generate admin tool

Options:
  -n, --name TEXT                 Admin tool name
  -atc, --admin-tool-config PATH  Admin tool configuration values
  --help                          Show this message and exit.
```
Exemple pour SuperCRUD: 
`./build -c path/to/my-vars.ini -cc path/to/my-cluster-vars.ini gen-admin-tool -n supercrud -atc path/to/my-admin-tool-vars.ini`
