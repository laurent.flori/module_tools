#!/usr/bin/env python3

from os import makedirs
from os.path import basename
from os.path import dirname
from os.path import isfile
from os.path import isdir
from os.path import join
from os.path import relpath

from glob import glob

from jinja2 import Environment
from jinja2 import FileSystemLoader

import configparser
import click

config = configparser.ConfigParser(interpolation=None)
tools_dir = dirname(__file__)
default_config = "vars.ini"
default_clusterconfig = "cluster-vars.ini"
default_output = "./install"


@click.group()
@click.option(
    "-c",
    "--configfile",
    "configfile",
    default=default_config,
    type=click.Path(exists=True),
    help=f"Configuration values (default: {default_config})",
)
@click.option(
    "-cc",
    "--clusterconfigfile",
    "clusterconfigfile",
    default=default_clusterconfig,
    type=click.Path(exists=True),
    help=f"Cluster configuration values (default: {default_clusterconfig})",
)
@click.option(
    "-o",
    "--outputdir",
    "outputdir",
    default=default_output,
    help=f"Output Directory (default: {default_output})",
)
@click.pass_context
def cli(ctx, configfile, clusterconfigfile, outputdir):
    ctx.ensure_object(dict)
    ctx.obj["config"] = configfile
    ctx.obj["clusterconfig"] = clusterconfigfile
    ctx.obj["outputdir"] = outputdir
    default_loader = FileSystemLoader(join(tools_dir, "templates"))
    default_env = Environment(loader=default_loader)
    ctx.obj["jinja_env"] = default_env


@cli.command()
@click.pass_context
def gen_socle(ctx):
    """
    Generate all templates for all services in outputdir
    """
    ctx.obj["output_path"] = ctx.obj["outputdir"]
    genTemplates(ctx)


@cli.command()
@click.option("-n", "--name", default=None, help="Addon name")
@click.option(
    "-ac",
    "--addon-config",
    type=click.Path(exists=True),
    help="Addon configuration values",
)
@click.pass_context
def gen_addon(ctx, name=None, addon_config=None):
    """
    Generate addon
    """
    genExtra(ctx, "addons", name, addon_config)


@cli.command()
@click.option("-n", "--name", default=None, help="Admin tool name")
@click.option(
    "-atc",
    "--admin-tool-config",
    type=click.Path(exists=True),
    help="Admin tool configuration values",
)
@click.pass_context
def gen_admin_tool(ctx, name=None, admin_tool_config=None):
    """
    Generate admin tool
    """
    genExtra(ctx, "admin-tools", name, admin_tool_config)


###
def genExtra(ctx, app_type, name, extra_config):
    """
    Generate extra config
    """
    if name is None:
        return False

    if extra_config is None:
        extra_config = join(app_type, name, f"{name}-vars.ini")

    root_dir = join(tools_dir, app_type, name)
    templates_dir = join(root_dir, "templates")
    extra_loader = FileSystemLoader([templates_dir, join(tools_dir, "templates")])
    jinja_env = ctx.obj["jinja_env"].overlay(loader=extra_loader)
    ctx.obj["jinja_env"] = jinja_env

    ctx.obj["output_path"] = join(ctx.obj["outputdir"], app_type, name)

    genTemplates(ctx, root_dir=root_dir, extra_config=extra_config)


###
def genTemplates(ctx=None, root_dir=None, extra_config=None):
    """
    Generate all templates
    """
    root_dir = root_dir or ""
    templates_root_dir = join(tools_dir, root_dir, "templates")
    template_dirs = [
        "templates",
        "templates/resources",
        "templates/scripts",
        "templates/docs",
    ]
    for template_dir in template_dirs:
        template_path = join(tools_dir, root_dir, template_dir)
        for template in glob(f"{template_path}/*"):
            if not isfile(template):
                continue

            template = relpath(template, templates_root_dir)
            click.echo(template)
            genTemplate(ctx, template=template, extra_config=extra_config)


def genTemplate(ctx=None, extra_config=None, template=None):
    """
    Generate and save a single template
    """
    stdout = template == "docs/README"

    if extra_config is None:
        config.read([ctx.obj["config"], ctx.obj["clusterconfig"]])
    else:
        config.read([ctx.obj["config"], ctx.obj["clusterconfig"], extra_config])

    env = ctx.obj["jinja_env"]
    tmpl = env.get_template(template)
    output = tmpl.render(config=config, output_dir=ctx.obj["output_path"])
    if stdout:
        click.echo(output)

    writeFile(
        filepath=ctx.obj["output_path"], filename=basename(template), filecontent=output
    )


def writeFile(filepath=None, filename=None, filecontent=None):
    """
    Write a file in a directory, create the directory if it does not exists
    """
    if not isdir(filepath):
        makedirs(filepath)
    with open(join(filepath, filename), "w") as output:
        output.write(filecontent)


if __name__ == "__main__":
    cli()
