# Parameters to connect to mongodb database
mongoName: {{ config['mongodb']['mongoName'] }}
mongoPort: "{{ config['mongodb']['mongoPort'] }}"
mongoDatabase: {{ config['mongodb']['mongoDatabase'] }}
mongoRsname: {{ config['mongodb']['mongoRsname'] }}
mongoUsername: {{ config['mongodb']['mongoUsername'] }}
mongoPassword: {{ config['mongodb']['mongoPassword'] }}

# Keycloak parameters
keycloak_url: {{ config['keycloak']['hostname'] }}.{{ config['general']['domain'] }}
keycloak_realm: {{ config['keycloak']['realm'] }} 

