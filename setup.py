from setuptools import setup, find_packages

setup(
    name='eole3',
    version='0.1.0',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'Click',
        'Jinja2',
    ],
    entry_points={
        'console_scripts': [
            'eole3-build = eole3.build:cli',
        ],
    },
)
